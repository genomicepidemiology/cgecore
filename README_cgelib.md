# CGELIB Documentation

The cgelib package contains different modules that provide with tools to run aligners (BLAST/KMA), read the outputs of these aligners, and standardize the results. These modules are used in the different Finders of the [Genomic Epidemiology Center](http://www.genomicepidemiology.org/).

- **alignment**: Contains classes to run and read aligners
- **applications**: Contains classes to run applications or executables
- **input**: Contains classes to generate the typical input for any tool with corresponding input check. Moreover it has resfinder specific classes
- **output**: Contains classes to standardize results
- **sequence**: Contains classes that are used to define biological sequences
- **utils**: Contains classes used across the other submodules
