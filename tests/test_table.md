# Antimicrobial Abbreviations

In ResFinder results where antimicrobial abbreviations are used, it will use the [EUCAST System for Antimicrobial Abbreviations](https://www.eucast.org/fileadmin/src/media/PDFs/EUCAST_files/Disk_test_documents/Disk_abbreviations/EUCAST_system_for_antimicrobial_abbreviations.pdf).

## Abbreviations

| Antimicrobial          | dummy | Abbreviation |
|------------------------|-------|--------------|
| Ceftiofur              |  1    | CTF          |
| Ceftobiprole           |  2    | CTO          |
| Ceftolozane-tazobactam |  3    | CTT          |

This text should be ignored.
