# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2024-12-05

### Added

- All functionality from the package [cgelib v.0.7.5](https://bitbucket.org/genomicepidemiology/cgelib). Beware that some parts of the added functionality is still in developoment and tests are for from complete.

### Changed

- pyproject.toml to be more in line with newer package standards for CGE python packages.

### Deprecated

- cgecore.alignment.py will be replaced by cgecore.alignment
- cgecore.argumentparsing will be replaced by cgecore.input
- cgecore.cgefinder will be cgecore.applications
- cgecore.utiliy will be replaced with cgecore.utils
- cgecore.blaster will be replaced with cgecore.applications
